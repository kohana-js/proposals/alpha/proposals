const {ORM} = require('kohanajs');

class User extends ORM{
  constructor(id, options) {
    super(id, options);

    //foreignKeys
    this.role_id = null

    //fields
    this.username = null;
    this.password = null;
  }
}

User.jointTablePrefix = 'user';
User.tableName = 'users';

User.fields = new Map([
  ["username", "String!"],
  ["password", "String!"]
]);

module.exports = User;
