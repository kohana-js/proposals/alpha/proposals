const {ControllerMixin} = require('kohanajs');

class ControllerMixinAdmin extends ControllerMixin{
  constructor(client, pathPrefix='admin/') {
    super(client);
    this.pathPrefix = pathPrefix;
  }

  async action_index(){
    const instances = this.client.mixin.get('instances');
    const view = this.client.mixin.get('view');
    const getView = this.client.mixin.get('getView');
    const model = this.client.model;

    const viewData = view.data;
    Object.assign(
      viewData,
      {items: instances, type: model}
    )
    this.client.tpl = getView(this.client.templates.index, viewData);
  }

  async action_read() {
    const view = this.client.mixin.get('view');
    const getView = this.client.mixin.get('getView');
    const readData = this.client.mixin.get('readData');

    this.client.tpl = getView(this.client.templates.read,
      Object.assign(view.data, readData)
    );
  }

  async action_create(){
    const getView = this.client.mixin.get('getView');
    const view = this.client.mixin.get('view');
    const readData = this.client.mixin.get('readData');

    this.client.tpl = getView(this.client.templates.create,
      Object.assign(view.data, readData)
    );
  }

  async action_delete(){
    const model = this.client.model;
    const id = this.client.mixin.get('id');

    if(!id){
      throw new Error(`500 / Delete ${model.name} require object id`);
    }

    if(!this.client.request.query['confirm']){
      const checkpoint = (this.client.mixin.get('$_GET').cp);
      const viewData = this.client.mixin.get('view').data;
      Object.assign(
        viewData,
        {
          title: `Please confirm to delete ${model.name} (${id})`,
          message : `Are you sure?`,
          cancelURL : checkpoint ? checkpoint : `/${this.pathPrefix}${model.tableName}`,
          confirmURL: `/${this.pathPrefix}${model.tableName}/delete/${id}?confirm=true` + (checkpoint? `&cp=${checkpoint}` : ''),
          label : 'Confirm',
        }
      )

      const getView = this.client.mixin.get('getView');
      this.client.tpl = getView(this.client.templates.dialog, viewData);
    }

  }

}
module.exports = ControllerMixinAdmin;