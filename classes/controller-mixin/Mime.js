const {ControllerMixin} = require('kohanajs');

class Mime extends ControllerMixin{
    constructor(client, headers) {
        super(client);
        this.headers = headers;
    }

    before(){
        const matchExtension = (/\.[0-9a-z]+($|\?)/i).exec(this.request.raw.url || '');

        const extension = matchExtension ? matchExtension[0].replace(/[.?]/g, '') : 'html';
        this.addBehaviour('fileExtension', extension);

        switch(extension){
            case 'js':
            case 'json':
                this.headers['Content-Type'] = 'application/json; charset=utf-8';
                break;
            case 'png':
                this.headers['Content-Type'] = 'image/png';
                break;
            case 'jpg':
            case 'jpeg':
                this.headers['Content-Type'] = 'image/jpeg';
                break;
            case 'html':
                this.headers['Content-Type'] = 'text/html; charset=utf-8';
                break;
            default:
                this.headers['Content-Type'] = 'text/html; charset=utf-8';
        }
    }
}

module.exports = Mime;