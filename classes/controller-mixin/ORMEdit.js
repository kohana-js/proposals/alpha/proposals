//prepare instance for edit view

const {KohanaJS, ControllerMixin, ORM} = require('kohanajs');
const HelperForm = KohanaJS.require('helper/Form');

class ControllerMixinORMEdit extends ControllerMixin{

  async before(){
    this.clientDB = this.client.mixin.get('db');
  }

  async action_edit(){
    const instance = this.client.mixin.get('instance');
    this.applyQueryValues(instance);

    this.addBehaviour('readData', Object.assign(
      this.getFieldData(instance),
      await this.getBelongsTo(instance),
      await this.getBelongsToMany(instance),
      await this.getHasMany(instance),
      this.getFormDestination(),
      this.getDomain()
    ));
  }

  applyQueryValues(instance){
    const $_GET = this.client.request.query || {};

    if($_GET['values']){
      const values = JSON.parse($_GET['values']);
      this.client.model.fields.forEach((v,k) =>{
        if(!values[k])return;
        if(instance[k] === null){
          instance[k] = values[k];
        }
      });
    }
  }

  async action_read(){
    await this.action_edit()
  }

  async action_create(){
    this.client.mixin.set('instance', ORM.create(this.client.model, {database : this.clientDB}));
    await this.action_edit();
  }

  getFormDestination(){
    const $_GET = this.client.request.query || {};
    if($_GET['cp']){
      return {
        destination : $_GET['cp']
      }
    }
    return {};
  }

  getFieldData(instance){
    const m = instance.constructor;

    return {
      title     : `${(instance.id) ? 'Edit': 'Create'} ${m.name}`,
      model     : m,
      item      : instance,
      fields    : [...m.fields].map(x => HelperForm.getFieldValue('', x[0], x[1], instance[x[0]])),
    };
  }

  async getBelongsTo(instance) {
    const m = instance.constructor;
    if (!m.belongsTo || m.belongsTo.length <= 0) return;

    const items = await Promise.all(
      [...m.belongsTo].map(async x => {
        const fk = x[0];
        const model = KohanaJS.require(ORM.prepend(x[1]));
        const items = await ORM.getAll(model, {database : this.clientDB});

        return {
          instance: instance,
          model: model,
          foreign_key: fk,
          items : items
        }
      })
    );

    return {
      belongsTo: items
    }
  }

  async getBelongsToMany(instance){
    const m = instance.constructor;
    if(!m.belongsToMany || m.belongsToMany.length <= 0)return;

    const items = await Promise.all(
      m.belongsToMany.map( async x => {
          const Model = KohanaJS.require(ORM.prepend(x));
          const values = await instance.belongsToMany(Model);
          const items = await ORM.getAll(Model, {database : this.clientDB});

          const itemsById = {};
          items.forEach(x => itemsById[x.id] = x);

          return{
            model : Model,
            values : values,
            items: items
          }
        }
      )
    );

    return { belongsToMany: items }
  }

  async getHasMany(instance){
    const m = instance.constructor;
    if(!m.hasMany || m.hasMany.length <=0 )return;

    const items = await Promise.all(
      m.hasMany.map(async x => {
        const fk = x[0];
        const Model = KohanaJS.require(ORM.prepend(x[1]));
        const fields = [...Model.fields].map(x => ({name:x[0], type: x[1].replace(/!$/, ''), required: /!$/.test(x[1])}));
        const items = await instance.hasMany(Model, fk);

        return {
          fk: fk,
          model : Model,
          fields: fields,
          items : items,
          defaultValues: encodeURIComponent(`{"${fk}":${this.client.mixin.get('id')}}`),
          checkpoint: encodeURIComponent(this.client.request.raw.url)
        }
      })
    );

    return { hasMany: items };
  }

  getDomain(){
    return {
      domain : this.client.request.hostname
    }
  }
}

module.exports = ControllerMixinORMEdit;