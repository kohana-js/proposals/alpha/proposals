const {ControllerMixin} = require('kohanajs');

class ControllerMixinLoginRequire extends ControllerMixin{
  constructor(controller, rejectLanding = '/') {
    super(controller);
    const request = this.client.request;

    if(!request.session || !request.session.admin_logged_in){
      return this.client.redirect(`${rejectLanding}?cp=${encodeURIComponent(request.raw.url)}`);
    }
  }
}

module.exports = ControllerMixinLoginRequire;