const path = require('path');
const {KohanaJS, Controller} = require('kohanajs');

KohanaJS.init();
KohanaJS.classPath.set('helper/Form.js', path.normalize(__dirname + '/../classes/helper/Form.js'));
const ControllerMixinMultipartForm = require('../classes/controller-mixin/MultipartForm');

class ControllerTest extends Controller{
  constructor(request) {
    super(request);
    this.addMixin(new ControllerMixinMultipartForm(this))
  }

  async action_index(){
    this.body = 'index';
  }
}

describe('multipart form test', ()=>{
  test('constructor', async()=>{
    const controller = new ControllerTest({});
    const result = await controller.execute();

    expect(result.status).toBe(200);
    expect(result.body).toBe('index');
  });

  test('request body object', async () => {
    const controller = new ControllerTest({
      headers:{},
      query:{
        'foo': 'bar',
      },
      body: {
        'ModelTest(2401110702891):name' : '111',
        'ModelTest(2401110710563):name' : '222'
      }});
    await controller.execute();
    const $_GET  = controller.mixin.get('$_GET');
    const $_POST = controller.mixin.get('$_POST');

    expect($_GET.foo).toBe('bar');
    expect($_POST['ModelTest(2401110702891):name']).toBe('111');
    expect($_POST['ModelTest(2401110710563):name']).toBe('222');
  })

  test('request query', async () => {
    const controller = new ControllerTest({
      headers:{},
      query:{},
      body: 'hello=world&five-demands=not-one-less',
    })
    await controller.execute();
    const $_POST = controller.mixin.get('$_POST');
    expect($_POST['hello']).toBe('world');
    expect($_POST['five-demands']).toBe('not-one-less');
  });

  test('file upload', async () => {
    
  })
})