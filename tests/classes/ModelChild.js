const {ORM} = require('kohanajs');

class ModelChild extends ORM{
  name=null;
  static joinTablePrefix = 'child';
  static tableName = 'children';

  static fields = new Map([
    ["name", "String!"]
  ]);

  static belongsTo = new Map([
    ["parent_id", "ModelParent"],
  ]);
}

module.exports = ModelChild;
