const {ORM} = require('kohanajs');

class ModelTest extends ORM{
  name=null;
  static joinTablePrefix = 'test';
  static tableName = 'tests';

  static fields = new Map([
    ["name", "String!"]
  ]);
}

module.exports = ModelTest;
